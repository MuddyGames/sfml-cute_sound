#include <Game.h>

#define CUTE_SOUND_IMPLEMENTATION
#include <cute_sound.h>

using namespace sf;

RenderWindow& window = RenderWindow(VideoMode(800, 600), "Launching...");;

Game::Game() : player(Player()), npc(NPC()){}

void Game::initialize()
{
	window.setSize(sf::Vector2u(640, 480));
	window.setTitle("Game");
}

void Game::run()
{
	initialize();

	cs_context_t* ctx = cs_make_context(window.getSystemHandle(), 44100, 8192, 0, NULL);
	cs_loaded_sound_t jump_audio = cs_load_wav("resources/sounds/select.wav");
	cs_playing_sound_t jump_instance = cs_make_playing_sound(&jump_audio);

	while (window.isOpen())
	{
		update();
		draw();
		cs_insert_sound(ctx, &jump_instance);
		cs_mix(ctx);
	}
	cs_free_sound(&jump_audio);
}

void Game::update()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();

		player.update();
		npc.update();
	}

}

void Game::draw()
{
	window.clear();
	//window.draw(shape);
	player.draw();
	npc.draw();
	window.display();
}


